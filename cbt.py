#!/usr/local/bin/python3.6
# noinspection PyPackageRequirements
import os
import re
from time import sleep

# noinspection PyPackageRequirements
from fabric.api import run, env
# noinspection PyPackageRequirements
from fabric.colors import green, yellow, red
# noinspection PyPackageRequirements
from fabric.contrib.files import contains, comment, uncomment
# noinspection PyPackageRequirements
from fabric.operations import prompt
# noinspection PyPackageRequirements
from fabric.tasks import execute

env.colorize_errors = True
env.command_timeout = 600
env.timeout = 600
env.connection_attempts = 10

env.user = 'root'
env.key_filename = '~/.ssh/id_rsa'
env.shell = "/bin/sh -c"


def refreshcbt(vmname):
    # get vm
    config = run(f"esxcli vm process list| grep -i {vmname} | grep -i 'config file'")
    print(config)


def poweroff(vm, vm_id):
    # check if running
    res = run(f"vim-cmd vmsvc/power.getstate {vm_id}", quiet=True)
    if 'powered on' in res.lower():
        print(yellow(f"PowerOff {vm}"))
        run(f"vim-cmd vmsvc/power.off {vm_id}", quiet=True)
        # check if still running
        running = True
        counter = 0
        while running:
            res = run(f"vim-cmd vmsvc/power.getstate {vm_id}", quiet=True)
            if 'powered on' in res.lower():
                counter += 1
                sleep(1)
            else:
                running = False
            if counter > 120:
                print(red(f"Problem stopping vm {vm}!"))
                return False
        print(green(f"{vm} Stopped!"))
        return True
    else:
        return True


def stopvm(vm, vm_id):
    # check if running
    res = run(f"vim-cmd vmsvc/power.getstate {vm_id}", quiet=True)
    if 'powered on' in res.lower():
        print(yellow(f"Stopping {vm}"))
        res = run(f"vim-cmd vmsvc/power.shutdown {vm_id}", warn_only=True, quiet=True)
        if 'vim.fault.ToolsUnavailable' in res:
            # poweroff instead
            return poweroff(vm, vm_id)

        if 'vmodl.fault.SystemError' in res:
            # poweroff instead
            return poweroff(vm, vm_id)

        # check if still running
        running = True
        counter = 0
        while running:
            res = run(f"vim-cmd vmsvc/power.getstate {vm_id}", quiet=True)
            if 'powered on' in res.lower():
                counter += 1
                sleep(1)
            else:
                running = False
            if counter > 120:
                print(f"Problem stopping vm {vm}!")
                return False
        print(green(f"{vm} Stopped!"))
        return True
    else:
        return True


def poweron(vm, vm_id):
    # check if running
    res = run(f"vim-cmd vmsvc/power.getstate {vm_id}", quiet=True)
    if 'powered off' in res.lower():
        print(yellow(f"Starting {vm}"))
        run(f"vim-cmd vmsvc/power.on {vm_id}", warn_only=True, quiet=True)

        # check if still running
        running = True
        counter = 0
        while running:
            res = run(f"vim-cmd vmsvc/power.getstate {vm_id}", quiet=True)
            if 'powered off' in res.lower():
                counter += 1
                sleep(1)
            else:
                running = False
            if counter > 120:
                print(f"Problem starting vm {vm}!")
                return False
        print(green(f"{vm} Started!"))
        return True
    else:
        return True


def getconfig(vm_id):
    res = run(f'vim-cmd vmsvc/get.datastores {vm_id} | grep "^url"', quiet=True)
    path = res.split()[1].strip()
    res = run(f'vim-cmd vmsvc/get.filelayout {vm_id} | grep vmPathName', quiet=True)
    path2, file = re.match('\w+\s=\s"\[\w+\]\s+(\w+)/([\w\s\(\)\-.]+)"', res).groups()
    fullpath = os.path.join(path, path2, file)
    return f'"{fullpath}"'


def choosehost():
    # seleziona host
    host_array = ['esxi-1', 'esxi-2', 'esxi-3', ]
    [print(f"{host_array.index(x)+1}. {x}") for x in host_array]
    vm_id = prompt(f"Choose Host: ")
    env.hosts = [host_array[int(vm_id) - 1]]
    execute(cbt, hosts=env.hosts)


def cbt():
    """ Main Operation """
    # load vm list
    proclist = run('vim-cmd vmsvc/getallvms', quiet=True)
    proclist_array = proclist.splitlines()
    vmlist = {}
    for i in proclist_array:
        if i.startswith('Vmid'):
            continue
        vmlist[i.split()[1]] = i.split()[0]

    vmlist_array = [x for x in vmlist]
    [print(f"{vmlist_array.index(x)+1}. {x}") for x in vmlist_array]
    vm_id = prompt(f"Choose VM Number: ")
    try:
        vm = vmlist_array[int(vm_id) - 1]
    except Exception as e:
        print(f"Invalid VM {e}")
        return False
    print(f"Chosen VM: {vm}")

    # check running
    running = False
    guest_status = run(f'vim-cmd vmsvc/get.guest {vmlist[vm]}', quiet=True)
    if 'guestState = "running"' in guest_status:
        running = True

    if not stopvm(vm, vmlist[vm]):
        return False

    configfile = getconfig(vmlist[vm])

    if contains(configfile, "ctkEnabled"):
        # remove ctk
        print(yellow(f"Disabling CBT on {vm}"))
        comment(configfile, ".*ctkEnabled.*", backup="")

        # power cycle
        poweron(vm, vmlist[vm])
        sleep(10)
        stopvm(vm, vmlist[vm])

        # enable ctk
        print(yellow(f"Enabling CBT on {vm}"))
        uncomment(configfile, ".*ctkEnabled.*", backup="")
        poweron(vm, vmlist[vm])

        if not running:
            print(yellow(f"{vm} was off, stopping..."))
            sleep(10)
            stopvm(vm, vmlist[vm])


    else:
        print(red("CBT not configured yet on this vm!"))

    another = prompt(f"Another ?: (Y/n)")
    if another != 'n':
        execute(cbt)


if __name__ == '__main__':
    execute(choosehost)
